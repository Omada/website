# Welcome to robots.txt, the place where shunning bots is encouraged.
# Humans are welcome to read. Bots are welcome to follow.
#
# Policy
#
# Allowed:
# - Search engine indexers (even google, though I hate it)
# - RSS Aggreggators (unless too aggressive)
# - Archival services
# - Fediverse federation stuff
#
# Disallowed:
# - Marketing or SEO crawlers
# - Agressive and annoying bots
# - Honeypots
#
# If your piece of sloppy code gets in this list, you contribute to the
# enshittification of the web and you should fuck off. Also stay the fuck
# away from me and my data, as well as from the users I host here.
#
# If your piece of shit software doesn't respect robots.txt, your IP will be blocked.
#
# If you have any questions, reach out to fluffery at autistici dot org.
# file was originally made by getimiskon at disroot dot org

# +-------------------+
# |                   |
# |   HALL OF SHAME   |
# |                   |
# +-------------------+

User-agent: *
Allow: /$
Allow: /
Disallow: *commit*
Disallow: /media_proxy/
Disallow: /interact/
Disallow: /api/v1/instance/domain_blocks

Crawl-delay: 2

# Marketing/SEO cancer
User-agent: AhrefsBot
Disallow: /
# I swear, I have to block this one from my Nginx settings, Fuck you.

# Search crawler
User-agent: ImagesiftBot
Disallow: /

# Marketing/SEO cancer
User-agent: dotbot
Disallow: /

User-agent: DotBot
Disallow: /

# Marketing/SEO cancer
User-agent: SemrushBot
Disallow: /

User-agent: SemrushBot-SA
Disallow: /

# 'Threat hunting' bullshit
User-agent: CensysInspect
Disallow: /

# Marketing/SEO
User-agent: rogerbot
Disallow: /

User-agent: BLEXBot
Disallow: /

# Huwei something or another, badly behaved
User-agent: AspiegelBot
Disallow: /

# Marketing/SEO
User-agent: ZoominfoBot
Disallow: /

# YandexBot is a dickhead, too aggressive
User-agent: Yandex
Disallow: /

# Marketing/SEO
User-agent: MJ12bot
Disallow: /

# Marketing/SEO
User-agent: DataForSeoBot
Disallow: /

# No
User-agent: turnitinbot
Disallow: /

User-agent: Turnitin
Disallow: /

# Does not respect * directives
User-agent: Seekport Crawler
Disallow: /

# Marketing
User-agent: serpstatbot
Disallow: /

# The example for img2dataset, although the default is *None*
User-agent: img2dataset
Disallow: /

# Brandwatch - "AI to discover new trends"
User-agent: magpie-crawler
Disallow: /

# webz.io - they sell data for training LLMs.
User-agent: Omgilibot
Disallow: /

# Items below were sourced from darkvisitors.com
# Categories included: "AI Data Scraper", "AI Assistant", "AI Search Crawler", "Undocumented AI Agent"

# AI Search Crawler
# https://darkvisitors.com/agents/amazonbot

User-agent: Amazonbot
Disallow: /

# Undocumented AI Agent
# https://darkvisitors.com/agents/anthropic-ai

User-agent: anthropic-ai
Disallow: /

# AI Search Crawler
# https://darkvisitors.com/agents/applebot

User-agent: Applebot
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/applebot-extended

User-agent: Applebot-Extended
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/bytespider

User-agent: Bytespider
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/ccbot

User-agent: CCBot
Disallow: /

# AI Assistant
# https://darkvisitors.com/agents/chatgpt-user

User-agent: ChatGPT-User
Disallow: /

# Undocumented AI Agent
# https://darkvisitors.com/agents/claude-web

User-agent: Claude-Web
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/claudebot

User-agent: ClaudeBot
Disallow: /

# Undocumented AI Agent
# https://darkvisitors.com/agents/cohere-ai

User-agent: cohere-ai
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/diffbot

User-agent: Diffbot
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/facebookbot

User-agent: FacebookBot
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/google-extended

User-agent: Google-Extended
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/gptbot

User-agent: GPTBot
Disallow: /

# AI Data Scraper
# https://darkvisitors.com/agents/omgili

User-agent: omgili
Disallow: /

# AI Search Crawler
# https://darkvisitors.com/agents/perplexitybot

User-agent: PerplexityBot
Disallow: /

# AI Search Crawler
# https://darkvisitors.com/agents/youbot

User-agent: YouBot
Disallow: /


#...................../´¯¯/)
#...................,/¯.../         +----------------------------------------+
#.................../..../          |                                        |
#.............../´¯/'..'/´¯¯`·¸     | To the creators of the shitbots above: |
#.........../'/.../..../....../¨¯\  |                                        |
#..........('(....´...´... ¯~/'..') |                FUCK YOU.               |
#...........\..............'...../  |       TOTAL COMMERCIAL WEB DEATH.      |
#............\....\.........._.·´   |                                        |
#.............\..............(      +----------------------------------------+
#..............\..............\

# The thing is that you know online hosting is NOT free.
# Yet you send requests to our servers and scraping our data without consent.
# By doing so, you add a lot of unnecessary work for us to block your bots.
# You're a disgrace. You are the reason the web is shit.
# You made the people being afraid of expressing themselves online.
# Congratulations. Enjoy your enshittified web until it collapses.

# This file is loosely based on the robots.txt file of sr.ht
# based off the robots.txt belonging to getimiskon
# additions from https://github.com/healsdata/ai-training-opt-out/blob/main/robots.txt and https://darkvisitors.com/

# to all of you: thank you
